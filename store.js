import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    temperatureIndex: 0,
    temperatureIncrement: 5,
    temperatureMin: 100,
    temperatureMax: 500,
    currentTimer: 0,
    mode: 'menu',
    precision: {
      connecting: false,
      ready: false,
    },
    status: {
      burner0: false,
      burner1: false,
      burner2: false,
      burner3: false,
      burner4: false,
      burner0Pos: 0,
      burner1Pos: 0,
      burner2Pos: 0,
      burner3Pos: 0,
      burner4Pos: 0,
      burner0Temperature: -1,
      burner1Temperature: -1,
      burner2Temperature: -1,
      burner3Temperature: -1,
      burner4Temperature: -1,
      error: false,
      pairing: false,
      paired: false,
      paired2: false,
      unpairconfirm: false,
      bluetooth: false,
      wifi: false,
      timer: false,
      sync: false,
      precision: false,
      heating: false,
      heating2: false,
      heated: false,
      heated2: false,
      lockall: false,
    },
  },

  getters: {
    isActive: (state) => (target) => {
      return state.status[target];
    },
    getMode: (state) => {
      return state.mode;
    },
    isActiveBurner: (state) => (target) => {
      return state.status['burner' + target];
    },
    getActiveBurners: (state) => {
      let actives = [];
      for (let i = 0; i < 5; i++) {
        if (state.status['burner' + i]) actives.push(i);
      }
      return actives;
    },
  },

  mutations: {
    setMode(state, payload) {
      state.mode = payload;
    },
    setStatus(state, payload) {
      state.status[payload.target] = payload.status;
    },
    setBurnerStatus(state, payload) {
      state.status['burner' + payload.burner] = payload.status;
    },
    setTimer(state, amount) {
      state.currentTimer = amount;
    },
    increaseTimer(state, amount) {
      state.currentTimer += amount;
    },
    decreaseTimer(state, amount) {
      state.currentTimer -= amount;
    },
  },

  actions: {
    active(context, payload) {
      context.commit('active', payload);
    },
    setMode(context, payload) {
      context.commit('setMode', payload);
    },
    setStatus(context, payload) {
      context.commit('setStatus', payload);
    },
    setBurnerStatus(context, payload) {
      context.commit('setBurnerStatus', payload);
    },
    setTemp(context, payload) {
      if (payload.amount < context.state.temperatureMax && payload.amount >= context.state.temperatureMin) {
        context.commit('setTemp', payload);
      }
    },
    setTimer(context, amount) {
      context.commit('setTimer', amount);
    },
    increaseTimer(context, amount) {
      context.commit('increaseTimer', amount);
    },
    decreaseTimer(context, amount) {
      if (context.state.currentTimer - amount >= 0) {
        context.commit('decreaseTimer', amount);
      } else {
        context.state.currentTimer = 0;
      }
    },
  },
});
