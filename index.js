import Vue from 'vue';
import App from './components/main.vue';
import SoundPlayer from './js/libs/Sounds';
import store from './store';
import gsap from 'gsap';
import './index.scss';
import Start from './aud/Start.mp3';
import Error from './aud/Error.mp3';
import Touch from './aud/Touch.mp3';
import Cancel from './aud/Cancel.mp3';

var scl = 1;
function onResize() {
  let el = document.querySelector('#wrapper');
  let dpi = document.getElementById('dpi').offsetWidth;
  const appWpx = 792;
  const appHpx = 432;

  const buttonCluster = 7.5369 * dpi;
  const cooktopW = 10.9936;
  const cooktopH = 6.0;
  const cooktopWpx = 10.9936 * dpi;
  const cooktopHpx = 6.0 * dpi;

  //width pixel ratio stSclW = window.innerWidth/appWpx
  //height pixel ratio stSclH = window.innerHeight/appHpx

  //width pixel ratio pxdSclW = window.innerWidth/cooktopWpx * window.devicePixelRatio
  //height pixel ratio pxdSclH = window.innerHeight/cooktopHpx * window.devicePixelRatio
  // pxdSclH XXX
  // scl = (window.innerHeight / cooktopHpx) * window.devicePixelRatio;

  // scl = window.devicePixelRatio == 1 ? 1.512826856837918 : 1.8128268568379182;

  scl =
    window.devicePixelRatio == 1
      ? 1.512826856837918
      : window.innerWidth > cooktopWpx + 20
      ? 1.8128268568379182
      : (window.innerWidth + 150) / buttonCluster;

  gsap.set(el, { scale: scl, force3D: false });
  console.log(`
    scl: ${scl}
    dpi: ${dpi}
    window.devicePixelRatio: ${window.devicePixelRatio}
    cooktop pixels: ${cooktopWpx} x ${cooktopHpx}
    deviceSize: ${appWpx} x ${appHpx}
    deviceSize (scaled): ${appWpx * scl} x ${appHpx * scl}
    window.innerWidth: ${window.innerWidth}
    window.innerHeight: ${window.innerHeight}
    // cooktop: ${cooktopW} x ${cooktopH}
  `);
}
function initSounds() {
  SoundPlayer.init({
    soundList: [
      {
        id: 'Start',
        src: Start,
        data: 1,
      },
      {
        id: 'Error',
        src: Error,
        data: 2,
      },
      {
        id: 'Touch',
        src: Touch,
        data: 3,
      },
      {
        id: 'Cancel',
        src: Cancel,
        data: 4,
      },
    ],
    loop: false,
  });
  window.SoundPlayer = SoundPlayer;
  window.removeEventListener('mousedown', initSounds.bind(this));
}

//for scaling
function initKeyScale() {
  document.body.addEventListener(
    'keydown',
    ((evt) => {
      let char = evt.keyCode;
      if (/38|40/i.test(char)) {
        evt.preventDefault();
        evt.stopPropagation();

        switch (char) {
          case 38: //up =
            scl += 0.1;
            break;

          case 40: //down =
            scl -= 0.1;
            break;
          default:
            break;
        }
        console.log(scl);
        gsap.set('#wrapper', { scale: scl, force3D: false });
      }
    }).bind(this)
  );
}
function init() {
  function preventBehavior(e) {
    e.preventDefault();
  }

  document.addEventListener('touchmove', preventBehavior, { passive: false });
  window.addEventListener(
    'resize',
    function () {
      setTimeout(onResize, 100);
      // onResize();
    }.bind(this)
  );
  onResize();
  window.addEventListener('mousedown', initSounds.bind(this));
  window.scl = scl;

  /* Use below to adjust size of container */
  // initKeyScale();
}
new Vue({
  store,
  render: (createElement) => createElement(App),
}).$mount('#app');

document.addEventListener('DOMContentLoaded', init);
