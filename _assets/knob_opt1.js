maxRotation = 250;
svgNode = document.getElementById('svg-node');
container = document.getElementById('container');
dialValue = document.getElementById('dial-value');
dialLineBg = document.getElementById('dial-line-bg');
info = document.getElementById('info');
nullObject = document.getElementById('null-object');
dialLine = document.getElementById('dial-line');
outline = document.getElementById('outline');

var rotationOffset = -145, //in case the dial's "home" position isn't at 0 degrees (pointing right). In this case, we use 90 degrees.
  RAD2DEG = 180 / Math.PI; //for converting radians to degrees

dialLine.percent = 0;

TweenMax.set(document.body, { userSelect: 'none' });
TweenMax.set(dialLine, { drawSVG: '0%', opacity: 0.5 });
TweenMax.set([container, svgNode, dialValue, info], {
  position: 'absolute',
  left: '50%',
  top: '50%',
  xPercent: -50,
  yPercent: -50,
});

TweenMax.set(nullObject, { position: 'absolute', x: 0 });
// TweenMax.set(dialLineBg, { position: 'absolute', opacity: 0.1 });
TweenMax.set(container, { transformOrigin: '50% 50%' });

var drag = Draggable.create(container, {
  //trigger:container,
  bounds: { maxRotation: maxRotation, minRotation: 0 },
  type: 'rotation',
  throwProps: true,
  //note: onPressInit was added in GSAP 2.0.2
  onPressInit: function (e) {
    //figure out the angle from the pointer to the rotational origin (in degrees)
    var rotation =
      Math.atan2(this.pointerY - this.rotationOrigin.y, this.pointerX - this.rotationOrigin.x) * RAD2DEG +
      rotationOffset;
    if (rotation < 0) {
      rotation += 360;
    } else if (rotation > 270) {
      rotation -= 360;
    }
    TweenLite.set(this.target, { rotation: rotation });
    animatedUpdate(rotation);
  },
  onDrag: instantUpdate,
  onThrowUpdate: instantUpdate,
  onRelease: function () {
    //otherwise, if we click and release immediately, there's momentum from the initial jump that'll cause the throwProps to kick in, but we don't want it to in this case.
    if (this.tween && this.timeSinceDrag() > 0.02) {
      this.tween.kill();
    }
  },
})[0];

function instantUpdate() {
  var percent = (drag.rotation / maxRotation) * 100;
  TweenMax.set(dialLine, { drawSVG: percent + '%', percent: percent });
  dialValue.innerHTML = Math.floor(percent);
}

function animatedUpdate(rotation) {
  var percent = (rotation / maxRotation) * 100;
  TweenMax.to(dialLine, 0.6, {
    drawSVG: percent + '%',
    percent: percent,
    onUpdate: function () {
      dialValue.innerHTML = Math.floor(dialLine.percent);
    },
  });
}
