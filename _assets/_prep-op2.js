(function() {
  var burner;
  var knobProxy;
  var dialValue;
  var dialLine;
  var drag;

  //in case the dial's "home" position isn't at 0 degrees (pointing right). In this case, we use 90 degrees.
  //   var rotation;
  const maxRotation = 275;
  const rotationOffset = -130;
  const stops = [0, 1, 3, 5, 8, 10, 14, 17, 21, 28, 29, 34, 41, 47, 54, 63, 72, 79, 82, 100];
  const RAD2DEG = 180 / Math.PI; //for converting radians to degrees
  function getStop(rotation) {
    var rotationSnap = Math.floor(stops.length * (rotation / 100));
    console.log(stop[rotationSnap]);
  }
  function instantUpdate() {
    var percent = getStop((drag.rotation / maxRotation) * 100);
    TweenMax.set(dialLine, { drawSVG: percent + '%', percent: percent });
  }

  function animatedUpdate(rotation) {
    var percent = getStop((rotation / maxRotation) * 100);
    TweenMax.to(dialLine, 1.0, {
      drawSVG: percent + '%',
      percent: percent,
      onUpdate: function() {
        showValue(Math.floor(dialLine.percent));
      },
    });
  }
  function init() {
    burner = document.getElementById('burner');
    knobProxy = document.getElementById('knobProxy');
    dialValue = document.getElementById('dial-value');
    dialLine = document.getElementById('burnerFill');
    dialLine.percent = 0;
    TweenMax.set(knobProxy, {
      userSelect: 'none',
      position: 'absolute',
      left: 114,
      top: 77,
      xPercent: -50,
      yPercent: -50,
      transformOrigin: '50% 50%',
    });
    TweenMax.set(burner, {
      position: 'absolute',
      left: '50%',
      top: '50%',
      xPercent: -50,
      yPercent: -50,
      transformOrigin: '50% 50%',
    });
    TweenMax.set(dialLine, { drawSVG: '0%' });
    TweenMax.set(knobProxy, { transformOrigin: '50% 50%' });

    drag = Draggable.create(knobProxy, {
      bounds: { maxRotation: maxRotation, minRotation: 0 },
      type: 'rotation',
      throwProps: false,
      onPressInit: function(e) {
        //figure out the angle from the pointer to the rotational origin (in degrees)
        var rotation =
          Math.atan2(this.pointerY - this.rotationOrigin.y, this.pointerX - this.rotationOrigin.x) * RAD2DEG +
          rotationOffset;
        if (rotation < 0) {
          rotation += 360;
        } else if (rotation > 270) {
          rotation -= 360;
        }
        console.log(rotation);
        TweenLite.set(this.target, { rotation: rotation });
        animatedUpdate(rotation);
      },
      onDrag: instantUpdate,
      onThrowUpdate: instantUpdate,
      onRelease: function() {
        if (this.tween && this.timeSinceDrag() > 0.02) {
          this.tween.kill();
        }
      },
    })[0];
  }
  document.addEventListener('DOMContentLoaded', init);
})();
