import Vue from 'vue';
/*
Vue.component('my-component', {
  components: {
    // Components that can be used in the template
    ProductComponent,
    ReviewComponent
  },
  props: {
    // The parameters the component accepts
    message: String,
    product: Object,
    }
  },
  data: function () {
    return {
      firstName: 'Vue',
      lastName: 'Mastery'
    }
  },
  computed: {
    // Return cached values until dependencies change
    fullName: function () {
      return this.firstName + ' ' + this.lastName
    }
  },
  watch: {
    // Called when firstName changes value
    firstName: function (value, oldValue) {}
  },
  methods: {},
  template: `<span>{{ message }}</span>`,
});
*/
Vue.component('burnerArt', {
  components: {},
  props: {
    active: {
      type: Boolean,
      required: true
    }
  },
  data: function() {
    return {};
  },
  computed: {},
  watch: {},
  methods: {},
  template: `<g><path
          id="burnerPlate"
          d="M113.7,1.3C71.9,1.3,38.1,35.2,38.1,77c0,41.8,33.9,75.6,75.6,75.6s75.6-33.9,75.6-75.6
    C189.3,35.2,155.5,1.3,113.7,1.3z M168.5,37.8l-10.6,8.4c-1.2-1.6-2.4-3.2-3.7-4.7l10.6-8.4C166.1,34.6,167.3,36.2,168.5,37.8z
     M163.5,31.6l-9.6,9.6c-1.3-1.4-2.7-2.8-4.1-4.1l9.6-9.6C160.8,28.8,162.1,30.2,163.5,31.6z M157.7,26.1l-8.4,10.6
    c-1.4-1.2-2.9-2.4-4.5-3.5l8.4-10.6C154.8,23.7,156.3,24.9,157.7,26.1z M151.6,21.4l-7.2,11.5c-1.5-1-3.1-2-4.7-2.9l7.1-11.5
    C148.5,19.4,150,20.3,151.6,21.4z M145,17.4l-5.8,12.2c-1.6-0.8-3.2-1.6-4.9-2.3l5.8-12.2C141.8,15.8,143.4,16.6,145,17.4z
     M138.1,14.3l-4.4,12.8c-1.6-0.6-3.3-1.2-4.9-1.7l4.4-12.8C134.8,13.1,136.4,13.7,138.1,14.3z M130.9,12l-2.9,13.2
    c-1.6-0.4-3.3-0.8-4.9-1.1l2.9-13.2C127.6,11.2,129.3,11.5,130.9,12z M118.7,10c1.6,0.1,3.3,0.3,4.9,0.5L122,23.9
    c-1.6-0.2-3.2-0.4-4.9-0.5L118.7,10z M111.3,9.8c0.8,0,1.6-0.1,2.4-0.1c0.8,0,1.6,0,2.4,0.1l0,13.5c-0.8,0-1.6-0.1-2.4-0.1
    c-0.8,0-1.6,0-2.4,0.1L111.3,9.8z M108.6,10l1.5,13.4c-1.5,0.1-3.1,0.3-4.6,0.5L104,10.5C105.5,10.2,107.1,10.1,108.6,10z
     M101.2,10.9l2.9,13.2c-1.5,0.3-2.9,0.6-4.3,1l-2.9-13.2C98.3,11.5,99.7,11.2,101.2,10.9z M93.8,12.7l4.4,12.8
    c-1.4,0.4-2.7,0.9-4,1.4l-4.4-12.8C91.1,13.6,92.5,13.1,93.8,12.7z M86.7,15.4l5.8,12.2c-1.3,0.5-2.5,1.1-3.7,1.8L83,17.2
    C84.2,16.5,85.5,15.9,86.7,15.4z M79.9,18.9L87,30.4c-1.2,0.7-2.3,1.3-3.4,2.1l-7.1-11.5C77.6,20.2,78.7,19.5,79.9,18.9z M73.4,23.1
    l8.4,10.6c-1,0.8-2,1.6-3,2.4l-8.4-10.6C71.4,24.7,72.3,23.9,73.4,23.1z M67.3,28.3l9.6,9.6c-0.9,0.8-1.8,1.7-2.6,2.6l-9.6-9.6
    C65.5,30,66.4,29.1,67.3,28.3z M61.9,34.1l10.6,8.4c-0.8,0.9-1.5,1.8-2.2,2.8l-10.6-8.4C60.4,35.9,61.1,35,61.9,34.1z M57.2,40.3
    l11.5,7.1c-0.6,0.9-1.2,1.9-1.8,2.9l-11.5-7.1C56,42.2,56.6,41.3,57.2,40.3z M53.4,47l12.2,5.8c-0.5,0.9-0.9,1.9-1.4,2.9L52,49.9
    C52.5,48.9,52.9,47.9,53.4,47z M50.4,53.9l12.8,4.4c-0.4,0.9-0.7,1.9-1,2.9l-12.8-4.4C49.7,55.9,50,54.9,50.4,53.9z M48.2,61.1
    l13.2,2.9c-0.2,0.9-0.4,1.9-0.6,2.8L47.5,64C47.7,63,47.9,62.1,48.2,61.1z M46.8,68.5L60.2,70c-0.1,0.9-0.2,1.8-0.3,2.7l-13.4-1.5
    C46.6,70.3,46.7,69.4,46.8,68.5z M46.2,77.3c0-0.4,0-0.8,0-1.3l13.5,0c0,0.4,0,0.8,0,1.3c0,0.4,0,0.9,0,1.3l-13.5,0
    C46.2,78.1,46.2,77.7,46.2,77.3z M46.5,83.5L59.9,82c0.1,0.8,0.2,1.6,0.3,2.4l-13.4,1.5C46.7,85.1,46.6,84.3,46.5,83.5z M47.6,90.9
    L60.8,88c0.2,0.7,0.3,1.5,0.5,2.2l-13.2,2.9C47.9,92.4,47.7,91.6,47.6,90.9z M49.5,98.2l12.8-4.4c0.2,0.7,0.4,1.3,0.7,2l-12.8,4.4
    C50,99.5,49.8,98.8,49.5,98.2z M52.3,105.2l12.2-5.8c0.3,0.6,0.5,1.2,0.8,1.8L53.1,107C52.8,106.4,52.6,105.8,52.3,105.2z
     M56.8,113.6c-0.3-0.5-0.7-1-1-1.5l11.5-7.1c0.3,0.5,0.6,1,1,1.5L56.8,113.6z M60.3,118.4l10.6-8.4c0.3,0.4,0.7,0.9,1,1.3l-10.6,8.4
    C60.9,119.3,60.6,118.9,60.3,118.4z M66.5,125.5c-0.4-0.4-0.7-0.7-1.1-1.1l9.6-9.6c0.3,0.4,0.7,0.7,1.1,1.1L66.5,125.5z
     M113.7,122.6c-25.2,0-45.6-20.4-45.6-45.6s20.4-45.6,45.6-45.6s45.6,20.4,45.6,45.6S138.9,122.6,113.7,122.6z M178.7,95.5l0,0.1
    c-0.7,2.4-1.5,4.7-2.4,7l0,0.1c-0.9,2.3-2,4.6-3.2,6.8c0,0,0,0,0,0c-1.2,2.2-2.6,4.4-4,6.5c0,0,0,0,0,0c-1.5,2.1-3.1,4.1-4.8,6.1
    c0,0,0,0,0,0c-0.9,1-1.8,1.9-2.7,2.9l-9.6-9.6c0.6-0.6,1.1-1.1,1.7-1.7c0.4-0.4,0.7-0.8,1.1-1.2c1-1.2,2-2.4,2.9-3.6
    c0.3-0.4,0.6-0.9,0.9-1.3c0.9-1.2,1.6-2.5,2.4-3.9c0.3-0.4,0.5-0.9,0.7-1.4c0.7-1.3,1.4-2.7,2-4.1c0.2-0.4,0.4-0.9,0.5-1.4
    c0.6-1.4,1.1-2.9,1.5-4.3c0.1-0.4,0.3-0.9,0.4-1.3c0.4-1.5,0.7-3,1-4.6l11,1.2l2.4,0.5C179.9,90.8,179.3,93.2,178.7,95.5z
     M181.1,81.1c-0.1,2.4-0.4,4.7-0.8,7l-2.5-0.3l-11-1.2c0.1-0.4,0.2-0.8,0.2-1.2c0.2-1.6,0.4-3.2,0.5-4.8c0-0.4,0-0.7,0.1-1.1
    c0-0.8,0.1-1.6,0.1-2.4c0-0.9,0-1.7-0.1-2.6c0-0.3,0-0.6-0.1-0.9c-0.1-1.7-0.3-3.4-0.6-5.1c0-0.2-0.1-0.5-0.1-0.7
    c-0.3-1.8-0.7-3.5-1.2-5.2l0,0c-0.1-0.2-0.1-0.4-0.2-0.5c-0.5-1.8-1.1-3.5-1.8-5.2c0-0.1-0.1-0.2-0.1-0.3c-0.7-1.8-1.6-3.5-2.5-5.2
    c0,0-0.1-0.1-0.1-0.1c-1-1.8-2-3.4-3.2-5.1l11.5-7.1c1.2,1.7,2.2,3.4,3.2,5.2l-10.4,6.4l11-5.2c1,1.8,1.9,3.7,2.7,5.7l-9.3,4.3
    l9.7-3.3c0.8,2,1.5,4,2.1,6.1l-8,2.7l8.2-1.8c0.6,2.1,1.1,4.3,1.4,6.4l-6.6,1.4l6.7-0.7c0.4,2.2,0.6,4.4,0.7,6.7l-5.2,0.6l5.2,0
    c0.1,1.1,0.1,2.3,0.1,3.4c0,1.2,0,2.3-0.1,3.5l-3.8,0L181.1,81.1z"
        />
        <path
          class="grey"
          d="M168.3,131.9l-1.1-1.1c29.5-29.5,29.5-77.6,0-107.1C153,9.4,134,1.5,113.7,1.5c-20.2,0-39.3,7.9-53.6,22.2
      c-29.5,29.5-29.5,77.6,0,107.1l-1.1,1.1C29,101.7,29,52.7,59.1,22.6C73.7,8,93.1,0,113.7,0s40,8,54.6,22.6
      C198.5,52.7,198.5,101.7,168.3,131.9z"
          id="outerRing"
        />
        <path
          v-show="active"
          id="burner_status_active"
          class="red"
          d="M145.9,109.6l-0.8-0.8c17.3-17.3,17.3-45.5,0-62.8
	c-17.3-17.3-45.5-17.3-62.8,0C65,63.3,65,91.5,82.3,108.8l-0.8,0.8c-17.8-17.8-17.8-46.7,0-64.5c17.8-17.8,46.7-17.8,64.5,0
	C163.7,62.9,163.7,91.9,145.9,109.6z"
        />
        <path
          id="buttonBG"
          class="grey"
          d="M133.6,54.6c-0.5-1.3-1.2-2.5-2-3.7c-1.6-2.3-3.5-4.3-5.8-5.8c-1.1-0.8-2.4-1.4-3.7-2
    c-2.6-1.1-5.4-1.7-8.4-1.7c-0.7,0-1.5,0-2.2,0.1c-2.2,0.2-4.3,0.8-6.2,1.6c-1.3,0.5-2.5,1.2-3.7,2c-0.6,0.4-1.1,0.8-1.7,1.2
    c-4.8,4-7.9,10-7.9,16.7v43.2v36c0,11.9,9.7,21.6,21.6,21.6c6.7,0,12.7-3.1,16.7-7.9c0.4-0.5,0.9-1.1,1.2-1.7c0.8-1.1,1.4-2.4,2-3.7
    c0.8-1.9,1.4-4,1.6-6.2c0.1-0.7,0.1-1.5,0.1-2.2v-36V63C135.3,60,134.7,57.2,133.6,54.6 M133.8,106.2c0,11.1-9,20.1-20.1,20.1
    s-20.1-9-20.1-20.1V63c0-11.1,9-20.1,20.1-20.1s20.1,9,20.1,20.1V106.2z"
        /></g>`,
});

Vue.component('heater-size-large', {
  components: {},
  props: {
    // The parameters the component accepts
    ringSize: Number,
  },
  data: function () {
    return {
      active: false,
      ringSize: 0,
      max: 3,
    };
  },
  watch: {},
  methods: {},
  template: `<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="33.5px" height="33.5px" overflow="visible"><style>.st0{fill:#ec1c24}.st1{fill:#676767}</style><path id="ring3_1_" class="st0" d="M19.3,3.5c0.2,0.4,0.3,0.9,0.3,1.4c0,0.3,0,0.6-0.1,0.9c4.9,1.2,8.6,5.7,8.6,11
	c0,6.3-5.1,11.3-11.3,11.3c-6.3,0-11.3-5.1-11.3-11.3c0-5.3,3.6-9.8,8.6-11c-0.1-0.3-0.1-0.6-0.1-0.9c0-0.5,0.1-1,0.3-1.4
	c-6.2,1.2-11,6.7-11,13.3c0,7.5,6.1,13.5,13.5,13.5c7.5,0,13.5-6,13.5-13.5C30.3,10.2,25.6,4.7,19.3,3.5z"/><path class="st0" d="M23.6,10c-1.4-1.4-3.1-2.2-4.8-2.6c0.6,0.5,0.9,1.3,0.9,2.1c0,0.4-0.1,0.7-0.2,1c2.4,1,4.1,3.5,4.1,6.2
		c0,3.8-3,6.8-6.8,6.8s-6.8-3-6.8-6.8c0-2.8,1.7-5.2,4.1-6.2c-0.1-0.3-0.2-0.7-0.2-1c0-0.8,0.4-1.6,0.9-2.1C13,7.7,11.3,8.6,9.9,10
		c-3.8,3.8-3.8,9.8,0,13.6s9.9,3.8,13.6,0C27.3,19.8,27.3,13.7,23.6,10z" id="ring2_1_"/><g id="ring_x5F_border_1_"><path class="st1" d="M16.8 8.1c-.8 0-1.4.6-1.4 1.4 0 .8.6 1.4 1.4 1.4.8 0 1.4-.6 1.4-1.4C18.2 8.7 17.5 8.1 16.8 8.1zM16.8 3.4c-.8 0-1.4.6-1.4 1.4 0 .8.6 1.4 1.4 1.4.8 0 1.4-.6 1.4-1.4C18.2 4.1 17.5 3.4 16.8 3.4z"/><path class="st1" d="M28.6,4.9C25.4,1.7,21.2,0,16.8,0C12.3,0,8.1,1.7,4.9,4.9c-6.5,6.5-6.5,17.2,0,23.7c3.3,3.3,7.6,4.9,11.8,4.9
		c2.2,0,4.3-0.4,6.3-1.2c2-0.8,3.9-2,5.5-3.7C35.1,22.1,35.1,11.4,28.6,4.9z M27.5,27.5c-5.9,5.9-15.6,6-21.6,0C0,21.6,0,11.9,6,6
		c2.9-2.9,6.7-4.5,10.8-4.5c4.1,0,7.9,1.6,10.8,4.5c3,3,4.5,6.9,4.5,10.8C32,20.7,30.5,24.6,27.5,27.5z"/><path class="st1" d="M16.8,12.7c-0.8,0-1.4,0.6-1.4,1.4s0.6,1.4,1.4,1.4c0.8,0,1.4-0.6,1.4-1.4S17.5,12.7,16.8,12.7z"/></g><path class="st0" d="M20.4,20.4c-2,2-5.3,2-7.4,0c-2-2-2-5.3,0-7.4c0.5-0.5,1.1-0.9,1.7-1.1c-0.6,0.5-0.9,1.3-0.9,2.2
		c0,1.6,1.3,2.9,2.9,2.9c1.6,0,2.9-1.3,2.9-2.9c0-0.8-0.4-1.6-0.9-2.2c0.6,0.2,1.2,0.6,1.7,1.1C22.5,15.1,22.5,18.4,20.4,20.4z" id="ring1_1_"/></svg>`,
});
