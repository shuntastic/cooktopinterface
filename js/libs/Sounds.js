import {Howl, Howler} from 'howler';

class SoundPlayer {
  constructor() {
    this.mutedByApp = false;
    this.mutedByUser = false;
    this.enabled = false;
    this.sounds = {};
    this.events = {
      change: [],
    };
  }

  init(props) {
    this.enabled = true;
    props.soundList.forEach((sound) => {
      if (sound.options) {
        this.sounds[sound.id] = new Howl({
          src: sound.src,
          ...sound.options,
        });
      } else {
        this.sounds[sound.id] = new Howl({
          loop:((sound.id == 'music') ? true : false),
          src: sound.src,
        });
      }
    });
  }

  mute(soundId = '') {
    if (!this.enabled) return;
    if (soundId === '') {
      this.mutedByUser = true;
      this.onMuteChanged();
      this.dispatchEvent('change');
    } else {
      this.sounds[soundId].mute(true);
    }
  }

  unmute(soundId = '') {
    if (!this.enabled) return;
    if (soundId === '') {
      this.mutedByUser = false;
      this.onMuteChanged();
      this.dispatchEvent('change');
    } else {
      this.sounds[soundId].mute(false);
    }
  }

  appMute() {
    if (!this.enabled) return;
    this.mutedByApp = true;
    this.onMuteChanged();
    this.dispatchEvent('change');
  }

  appUnmute() {
    if (!this.enabled) return;
    this.mutedByApp = false;
    this.onMuteChanged();
    this.dispatchEvent('change');
  }

  onMuteChanged() {
    if (!this.enabled) return;
    Howler.mute(this.mutedByApp || this.mutedByUser);
  }

  toggleMute() {
    if (!this.enabled) return;
    this[this.mutedByUser ? 'unmute' : 'mute']();
  }

  playSound(soundId) {
    if (!this.enabled) return;
    if(soundId == 'music'){

    }
    this.sounds[soundId].play();
  }

  pauseSound(soundId) {
    if (!this.enabled) return;
    this.sounds[soundId].pause();
  }

  stopSound(soundId) {
    if (!this.enabled) return;
    this.sounds[soundId].stop();
  }

  isMuted() {
    return {
      muted: this.mutedByUser || this.mutedByApp,
      byUser: this.mutedByUser,
      byApp: this.mutedByApp,
    };
  }

  addEventListener(event, handler) {
    try {
      if (!this.events[event]) {
        throw new Error('no-event');
      }

      this.events[event].push(handler);
    } catch (error) {
      if (error.message === 'no-event') {
        console.log(`No event handler for: ${event}`);
      }
    }
  }

  dispatchEvent(event) {
    this.events[event].forEach((handle) => {
      handle({
        muted: this.mutedByUser || this.mutedByApp,
        byUser: this.mutedByUser,
        byApp: this.mutedByApp,
      });
    });
  }
}
export default new SoundPlayer();
